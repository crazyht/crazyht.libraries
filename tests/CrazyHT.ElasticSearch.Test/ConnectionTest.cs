﻿using CrazyHT.ElasticSearch.Connections;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace CrazyHT.ElasticSearch.Test
{
  public class ConnectionTest
  {
    [Fact]
    public void CanConnectToSingleServer()
    {
      IElasticSearchConnection cnx = new ElasticSearchConnection("http://localhost:9200");
      Assert.NotNull(cnx);

    }
  }
}
