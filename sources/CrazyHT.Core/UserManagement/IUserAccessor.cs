﻿using System.Threading.Tasks;

namespace CrazyHT.Core.UserManagement
{
  /// <summary>
  /// Interface for access current user
  /// </summary>
  /// <typeparam name="TUserKey">The type of the user key.</typeparam>
  public interface IUserAccessor<TUserKey>
  {
    /// <summary>
    /// Gets the current user.
    /// </summary>
    /// <returns>
    /// The current user
    /// </returns>
    Task<IUserInfo<TUserKey>> GetCurrent();
  }
}
