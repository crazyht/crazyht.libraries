﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CrazyHT.Core.UserManagement
{
  public interface IUserInfo<TKey>
  {
    public TKey Id { get; }
    public string DisplayName { get; }
    public string Email { get; }
  }
}
